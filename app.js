// ============================= Load internal module ======================= //

const config = require("_/config");
const logger = require("_/logger").logger;

// ================================= End ==================================== //

const postgresDbConnection = (async () => {
  try {
    config.configDB.connectRedis(config.env, (err, res) => {
      if (err) {
        logger.error("Error in redis connection");
        return;
      }
      console.log("redis connected");
      logger.info(`redis connected${res}`);
      // load external modules
      const express = require("express");
      // init express app
      const app = express();
      // set server home directory
      app.locals.rootDir = __dirname;
      // config express
      config.expressConfig(app, config.env);
      // attach the routes to the app
      require("_/routes")(app);
      // start server
      app.listen(config.env.appPort, () => {
        console.log(`Express server listening on ${config.env.appPort}, in ${config.env.TAG} mode`);
        logger.info(`Express server listening on ${config.env.appPort}, in ${config.env.TAG} mode`);
      });
    });
  } catch (e) {
    console.log("error", e);
    logger.error(e, "Error in connection postgres db");
  }
})();

  

