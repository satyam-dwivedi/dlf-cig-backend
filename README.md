################ Project Title #######################

*dlf-fms is an application used to complete the fitout process of a leased site in DLF.

################ Prerequisites #######################

* Editors needed webstorm / sublime

* Node/NPM should be installed on machine
* Node version 12.16.1 is needed

########### Installation command ##################
* sudo apt-get update
* sudo apt-get install build-essential libssl-dev
* curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | sh
* source ~/.profile
* nvm ls-remote
* nvm install 8.9.4

################ END ###############################

* Redis cache db should be installed on machine
########### Installation command ##################

* sudo apt-get install build-essential
* sudo apt-get install tcl8.5
* wget http://download.redis.io/releases/redis-stable.tar.gz
* tar xzf redis-stable.tar.gz
* cd redis-stable
* make
* make test
* make install
* cd utils
* sudo ./install_server.sh

################ END ###############################

* mysql should be installed on machine
#####################Installation command##################

* sudo apt-get update
* sudo apt-get install mysql
* CREATE USER root WITH PASSWORD 'root';
* ALTER USER root WITH SUPERUSER;
* create database dlf_fms;
* GRANT ALL PRIVILEGES ON DATABASE dlf-fms to root;


#######################################END#####################################

################## Repos Link to clone ####################
* git clone git@bitbucket.org:AmitAlsisaria/dlf-cig-backend.git

######################### Installing #############################

* Go to your project base directory and run command on terminal to start project
* npm start

######################## Running the tests ##########################
* Code is being test through vscode debugger
