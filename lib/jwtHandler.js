// ====================== Load external module ====================== //

// ========================== End =================================== //

// ============================ Load internal module ================= //

// const jwt = require("jsonwebtoken");
// const exceptions = require("_/customExceptions");
// const JWT_SECRET_KEY = require("_/config").env.JWT_SECRET.KEY;
// const appUtils = require("_/appUtils");

// // ============================= End ================================= //

// // Generate token
// const generateToken = async (data,messages) => {
//   try {
//     return await jwt.sign(data, JWT_SECRET_KEY);
//   } catch (err) {
//     throw exceptions.intrnlSrvrErr(messages.tokenGenError);
//   }
// };

// // Verify token
// const verifyToken = async (data) => {
//   try {
//     return await jwt.verify(data.token, JWT_SECRET_KEY);
//   } catch (err) {
//    let messages = appUtils.getAppLanguageMessages(data.app_language);
//     throw exceptions.unAuthenticatedAccess(messages.unauthorized);
//   }
// };

// // Export module
// module.exports = {
//   generateToken,
//   verifyToken,
// };

// ============================ Load internal module ================= //

const jwt = require("jsonwebtoken");
const exceptions = require("_/customExceptions");
const JWT_SECRET_KEY = require("_/config").env.JWT_SECRET.KEY;
const appUtils = require("_/appUtils");
const constants = require("_/constants");
const redisClient = require("_/redis/redis");
const crypto = require("crypto");
const messages = require("_/messages");

// ============================= End ================================= //

// Generate token
const generateToken = async (data, messages) => {
  try {
    const jwtToken = jwt.sign(data, JWT_SECRET_KEY);
   // const hashKey = crypto.randomBytes(20).toString("hex");
    const params = {};
  //  params.userToken = hashKey;
    params.jwtToken = jwtToken;
    params.id = data.id;
   // params.role = data.role;
    await redisClient.setJWTToken(params);
    return jwtToken;
  } catch (err) {
    throw exceptions.intrnlSrvrErr(messages.tokenGenException);
  }
};


const verifyToken = async (data) => {
  try {
    const resp = await redisClient.getValue(data.token);
    if (!resp)
      throw exceptions.unAuthenticatedAccess(messages.unauthorized , constants.STATUS_CODE.INVALID_TOKEN)
    // let tokenDetails = JSON.parse(resp);
    // const jwtToken = await redisClient.getValue(tokenDetails.id);
    // if (jwtToken === tokenDetails.jwtToken)
      return await jwt.verify(data.token, JWT_SECRET_KEY);
    // else {
    //   await redisClient.deleteKey(data.token);
    //   throw exceptions.unAuthenticatedAccess(messages.sessionExpired ,constants.STATUS_CODE.INVALID_TOKEN);
    // }
  } catch (e) {
    throw e;
  }
};

// Export module
module.exports = {
  generateToken,
  verifyToken,
};