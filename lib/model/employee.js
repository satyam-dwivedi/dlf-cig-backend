const sequelize = require("_/config/dbConfig").sequelize;
const DataType = require("sequelize");
const { STATUS } = require("_/constants");


const Employees = sequelize.define('employees', {
    email: {
        type: DataType.STRING,
        unique: true
    },
    name: {
        type: DataType.STRING
    },
    status: {
        type: DataType.TINYINT,
        default: STATUS.ACTIVE,
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    }
},
    {   
        underscored: true,
    });
Employees.sync({ alter: false });
module.exports = Employees;