const sequelize = require("_/config/dbConfig").sequelize;
const DataType = require("sequelize");
const {STATUS,CONSULTANT_TYPE} = require("_/constants");

const User = sequelize.define('users', {
    email: {
        type: DataType.STRING,
        unique: true
    },
    name:{
        type:DataType.STRING,
    },
    phone: {
        type: DataType.STRING
    },
    roles_id: {
        type: DataType.INTEGER
    },
    type: {
        type: DataType.INTEGER,
        defaultValue: true
    },
    project_id: {
        type: DataType.INTEGER,
    },
    otp: {
        type: DataType.INTEGER
    },
    otp_sent_time: {
        type: DataType.DATE
    },
    status:{
        type:DataType.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    }
},
    {
        underscored: true,
    });

User.sync({ alter: false });
module.exports = User;

