const Projects = require("_/model/projects");
const Towers = require("_/model/towers");
const Floors = require("_/model/floors");
const Properties = require("_/model/properties");
const Users = require("_/model/users");
const Customer = require("_/model/customer");

module.exports = {
    Projects,
    Towers,
    Floors,
    Properties,
    Users,
    Customer,
}
