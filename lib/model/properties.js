const sequelize = require("_/config/dbConfig").sequelize;
const Datatype = require("sequelize");
const Models = require("_/model");
const {STATUS} = require("_/constants");

const Property = sequelize.define('property',{
    name: {
        type: Datatype.STRING,
        allowNull: false
    },
    floor_id: {
        type: Datatype.INTEGER,
    },
    unit_type: {
        type: Datatype.STRING,
        allowNull: true
    },
    status: {
        type: Datatype.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: Datatype.INTEGER
    },
    updated_by: {
        type: Datatype.INTEGER
    }
},{
    indexes: [
        {
            unique: true,
            fields: ['floor_id', 'name']
        }
    ],
    underscored: true
});

Property.sync({alter: false});
module.exports = Property;