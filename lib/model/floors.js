const sequelize = require('_/config/dbConfig').sequelize;
const DataType = require('sequelize');
const { STATUS } = require('_/constants');

const Floor = sequelize.define('floor',{

    name: {
        type: DataType.STRING
    },
    tower_id: {
        type: DataType.INTEGER,
    },
    status: {
        type: DataType.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    }
},
{

    indexes: [
        {
            unique: true,
            fields: ['tower_id', 'name']
        }
    ],
    underscored: true,
});

Floor.sync({alter: false});
module.exports = Floor;
