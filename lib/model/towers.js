const sequelize = require('_/config/dbConfig').sequelize;
const DataType = require('sequelize');
const { STATUS } = require('_/constants');
const {Projects} = require('_/model');

const Tower = sequelize.define('tower',{

    project_id: {
        type: DataType.INTEGER,
    },
    name: {
        type: DataType.STRING,
    },
    floor_count: {
        type: DataType.TINYINT,
        allowNull: true
    },
    status: {
        type: DataType.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    }
},
{
    indexes: [
        {
            unique: true,
            fields: ['project_id', 'name']
        }
    ],
    timestamps:true,
    underscored: true,
});


Tower.sync({alter: false});
module.exports = Tower;
