const sequelize = require("_/config/dbConfig").sequelize;
const DataType = require("sequelize");
const {STATUS} = require("_/constants");

const Project = sequelize.define('project',{

    name: {
        type: DataType.STRING,
        unique: true,
        allowNull: true
    },
    manager_name: {
        type: DataType.STRING,
    },
    manager_email: {
        type: DataType.STRING,
        unique: true
    },
    scheme_code: {
        type: DataType.STRING
    },
    type: {
        type: DataType.INTEGER,
        allowNull: true
    },
    tower_count:{
        type: DataType.TINYINT,
        defaultValue: 1,
    },
    status: {
        type: DataType.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    }
},
{
    indexes: [{
        unique: true,
        fields: ['name','type']
    }],
    underscored: true,
});

Project.sync({alter: false});
module.exports = Project;