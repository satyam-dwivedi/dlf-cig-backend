const sequelize = require("_/config/dbConfig").sequelize;
const DataType = require("sequelize");
const {STATUS,CONSULTANT_TYPE} = require("_/constants");

const Customer = sequelize.define('customer', {
    name:{
        type:DataType.STRING,
    },
    phone: {
        type: DataType.STRING,
    },
    first_preference:{
        type:DataType.INTEGER,
    },
    second_preference:{
        type:DataType.INTEGER,
    },
    closing_manager: {
        type:DataType.STRING,
    },
    broker_name:{
        type:DataType.STRING,
    },
    last_four_digit:{
        type:DataType.INTEGER,
    },
    sourcing:{
        type:DataType.STRING,
    },
    value_in_crs:{
        type:DataType.FLOAT,
    },
    status:{
        type:DataType.TINYINT,
        defaultValue: STATUS.ACTIVE
    },
    created_by: {
        type: DataType.INTEGER
    },
    updated_by: {
        type: DataType.INTEGER
    },
    alloted_unit: {
        type: DataType.INTEGER
    },
    alloted_by: {
        type: DataType.INTEGER
    },
    alloted_at: {
        type: DataType.DATE
    },
    sold: {
        type: DataType.TINYINT(1),
        defaultValue: 0
    },
    remarks: {
        type: DataType.STRING
    }
},
    {
        underscored: true,
    });

Customer.sync({ alter: false});
module.exports = Customer;

