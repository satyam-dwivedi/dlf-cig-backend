

//= ========================= Load Modules Start ==========================

//= ========================= Load External Modules ==========================

//= ========================= Load Internal Modules ==========================

const appConst = require("_/constants");

//= ========================= Load Modules End ==========================

//= ========================= Class Definitions Start =====================

class APIResponse {
  constructor(sc, result) {
    this.sc = sc;
    if (sc == appConst.STATUS_CODE.SUCCESS) {
      result ? this.result = result : {};
    } else {
      result ? this.error = result : {};
    }
    this.time = new Date().getTime();
  }
}

//= ========================= Class Definitions End =======================

//= ========================= Export module start ==================================

module.exports = APIResponse;

//= ========================= Export module end ==================================
