
//= ========================= Load Modules Start ===========================

//= ========================= Load external Module =========================

//= ========================= Load internal Module =========================

const constants = require("_/constants");
const jwtHandler = require("_/jwtHandler");
const exceptions = require("_/customExceptions");
const appUtils = require("_/appUtils");
const userDao = require("_/dao/userDao");
const messages = require("_/messages").messages;

//= ========================= Load Modules End =============================

const checkUserStatus = (user, appLanguage) => {
  const errorMessages = appUtils.getAppLanguageMessages(appLanguage);
  if (user.status === constants.STATUS.INACTIVE)
    throw exceptions.unAuthenticatedAccess(messages.inactiveAccount);
  if (user.status === constants.STATUS.REJECTED)
    throw exceptions.unAuthenticatedAccess(errorMessages.rejectedAccount);
  if (user.status === constants.STATUS.UNVERIFIED)
    throw exceptions.unAuthenticatedAccess(errorMessages.unverifiedAccount);
  if (user.status === constants.STATUS.DELETED)
    throw exceptions.unAuthenticatedAccess(errorMessages.deletedAccount);
};

// This function is used to authenticate and authorize token
const authenticateAndAuthorizeToken = (authorizeRoles = []) => {
  return async function (req, res, next) {
    try {
      const jwtToken = req.get("authorization");
      const data = { token: jwtToken };
      console.log(jwtToken, "Tokennnn");
      req.user = await jwtHandler.verifyToken(data);
      console.log( authorizeRoles,"Roles Access Wanted",req.user, "TokenHiddednInfo");
      const resp = await userDao.getUserDetails(req.user);
      if (resp.id === req.user.id) {
        if (resp.status === constants.STATUS.INACTIVE)
          throw exceptions.unAuthenticatedAccess(messages.inactiveAccount);
        if (!resp) {
          throw exceptions.unAuthenticatedAccess(messages.invalid_token);
        }
        req.user.email = resp.email;
        // req.project_id = resp.project_id;
        //   checkUserStatus(resp, req.headers.app_language);
        next();
      }
      else {
        throw exceptions.unAuthenticatedAccess(messages.unauthorized);
      }
    } catch (e) {
      next(e);
    }
  }
};

//= ========================= Export Module Start ===========================

module.exports = {
  authenticateAndAuthorizeToken,
};

//= ========================= Export Module End ===========================

