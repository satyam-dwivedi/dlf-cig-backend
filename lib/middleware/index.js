
//= ========================= Load Modules Start ===========================

//= ========================= Load internal Module =========================

const authentication = require("_/middleware/authentication"),
  validators = require("_/middleware/validators"),
  multer = require("_/middleware/multer");

//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

module.exports = {
  validators,
  authentication,
  multer,
};
//= ========================= Export module end ==================================
