//= ========================= Load Modules Start ===========================

//= ========================= Load External Module =========================

const multer = require("multer");
const _ = require('lodash');
const {commonConstants} = require("_/constants")

//= ========================= Load Internal Module =========================

//= ========================= Load Modules End =============================

//= ========================= Export Module Start ===========================

// define disk storage strategy for multer
const storage = multer.diskStorage({
  destination(req, file, callback) {
    callback(null, "./upload");
  },
  filename(req, file, callback) {
    let ampFreeFilename = file.originalname && file.originalname.replace(/&/g,'_');
    console.log('uploaded file name:',ampFreeFilename);
    callback(null, `${file.fieldname}_${Date.now()}_${ampFreeFilename}`);
  },
});

const upload = multer({
  storage,
  // limits: { fileSize: commonConstants.MAX_FILE_SIZE }
});

const singleFile = function (key) {
  return upload.single(key);

  // try {
  //   return upload.single(key);
  // } catch (error) {
  //   console.log(error,"eerrrrrr")
  //   throw error;
  // }
};

const multiFile = function (key,limit = 10) {
  return upload.array(key,limit);

  // try {
  // return upload.array(key,limit);
  // } catch (error) {
  //   console.log(error,"eerrrrrr")
  //   throw error;
  // }
};

// Export modules
module.exports = {
  singleFile,
  multiFile,
};
