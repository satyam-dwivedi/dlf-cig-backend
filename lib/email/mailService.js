const nodemailer = require('nodemailer');
const path = require("path");
const config = require("_/config").env;

const mailService = {};


mailService.send = function (params) {
  let imagePath = path.join(__dirname, '../../views/images/dlf_logo.jpg');
  const transporter = nodemailer.createTransport({
    host: config.mailer.host,
    service: config.mailer.service,
    auth: {
      user: config.mailer.user,
      pass: config.mailer.pass
    }
  });
  const mailOptions = {
    from: config.mailer.from,
    to: params.email,
    cc: params.cc,
    subject: params.subject,
    html: params.html,
    attachments: [{
      filename: 'dlf_logo.jpg',
      path: imagePath,
      cid: 'logo'
    }]
  };
  transporter.sendMail(mailOptions, function (err, info) {
    if (err)
      console.log(err)
    else
      console.log(info);
  });
}

module.exports = mailService;

