const masterDataRouter = require("express").Router();
const resHndlr = require("_/resHandler");
const middleware = require("_/middleware");
const masterDataController = require("_/controllers/masterDataController");
const { ROLES } = require("_/constants");


/*
*get master data of different tables
*/
masterDataRouter.route("/get-master-data")
  .get([middleware.authentication.authenticateAndAuthorizeToken()],async (req, res, next) => {
    try {
      req.query.userId = req.user.id;
      req.query.roles = req.user.roles;
      console.log('----------->>>>>>>>>>>>>>>',req.query);
      const result = await masterDataController.getMasterData(req.query);
      resHndlr.sendSuccess(res, result);
    } catch (error) {
      resHndlr.sendError(res, error);
    }
  })

/*
*create master data of different tables
*/
masterDataRouter.route("/create-master-data")
  .post([middleware.authentication.authenticateAndAuthorizeToken()],async (req, res, next) => {
    try {
      req.body.created_by = req.body.updated_by = req.user.id
      const result = await masterDataController.createMasterData(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    } catch (error) {
      resHndlr.sendError(res, error);
    }
  })

/*
*update master data of different tables
*/
masterDataRouter.route("/update-master-data")
  .put([middleware.authentication.authenticateAndAuthorizeToken()],async (req, res, next) => {
    try {
      req.body.updated_by = req.user.id;
      const result = await masterDataController.updateMasterData(req.body);
      resHndlr.sendSuccess(res, result);
    } catch (error) {
      resHndlr.sendError(res, error);
    }
  })

/*
*activate/deactivate different masters data like consultants
*/
masterDataRouter.route("/update-status")
  .patch([middleware.authentication.authenticateAndAuthorizeToken()],async (req, res, next) => {
    try {
      req.body.userId = req.user.id;
      req.body.roles = req.user.roles;
      const result = await masterDataController.updateStatus(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    } catch (error) {
      resHndlr.sendError(res, error);
    }
  })

//upload document
masterDataRouter.route("/upload-document")
  .post([middleware.multer.singleFile("document")], async (req, res, next) => {
    try {
      // req.body.updated_by = req.user.id;
      console.log(req.file, "filelelele");
      const result = await masterDataController.uploadDocument(req.file);
      resHndlr.sendSuccess(res, req.file);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

/*
*create system users of different roles
*/
masterDataRouter.route("/create-system-user")
  .post([middleware.authentication.authenticateAndAuthorizeToken()],async (req, res, next) => {
    try {
      req.body.created_by = req.body.updated_by = req.user.id
      const result = await masterDataController.createSystemUser(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    } catch (error) {
      resHndlr.sendError(res, error);
    }
  })







module.exports = masterDataRouter;
