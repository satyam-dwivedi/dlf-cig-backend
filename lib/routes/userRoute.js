//= ========================= Load Modules Start =======================

const _ = require("lodash");
const userRouter = require("express").Router();
const resHndlr = require("_/resHandler");
const middleware = require("_/middleware");
const userController = require("_/controllers/userController");
const exceptions = require("_/customExceptions");
const appUtils = require("_/appUtils");
const messages = require("_/messages").messages;
const { ROLES } = require("_/constants");


// ======================== End ======================================== //

/**
 * send otp for login
 */

userRouter.route("/send-login-otp")
  .post(async (req, res, next) => {
    try {
      const result = await userController.sendLoginOtp(req.body);
      resHndlr.sendSuccess(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

userRouter.route("/login")
  .post(async (req, res, next) => {
    try {
      const result = await userController.login(req.body);
      resHndlr.sendSuccess(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

/**
 * Route to logout 
 */
userRouter.route("/logout")
  .delete([middleware.authentication.authenticateAndAuthorizeToken([ROLES.TENANT, ROLES.FMS, ROLES.REVIEWER, ROLES.CONSULTANT, ROLES.ADMIN, ROLES.CRM])], async (req, res) => {
    try {
      req.body.token = req.get("authorization");
      const result = await userController.logout(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    } catch (err) {
      resHndlr.sendError(res, err);
    }
  });

/**
 * Route to add user 
 */
userRouter.route("/add-user")
  .post(
    async (req, res) => {
      try {
        //  req.body.token = req.get("authorization");
        const result = await userController.addUser(req.body);
        resHndlr.sendSuccess(res, result);
      } catch (err) {
        resHndlr.sendError(res, err);
      }
    });


/**
* Route to get User Details
*/
userRouter.route("/get-user-details")
  .get([middleware.authentication.authenticateAndAuthorizeToken([ROLES.TENANT, ROLES.FMS, ROLES.CONSULTANT, ROLES.REVIEWER, ROLES.ADMIN, ROLES.READ_ONLY, ROLES.CRM])], async (req, res) => {
    try {
      req.body.id = req.user.id;
      const result = await userController.getUserDetails(req.body);
      resHndlr.sendSuccess(res, result);
    } catch (err) {
      resHndlr.sendError(res, err);
    }
  });

//upload documents
userRouter.route("/upload-documents")
  .post([middleware.multer.multiFile("documents")], async (req, res, next) => {
    try {
      appUtils.fileValidation(req);
      resHndlr.sendSuccess(res, req.files);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

//download document
userRouter.route("/download-document")
  .get((req, res, next) => {
    try {
      const result = userController.downloadDocument(req.query, res);
      //  res.download(result);
      res.download(result, (err) => {
        if (err) {
          let error = {
            errorCode: 2,
            message: messages.fileNotFound
          }
          return resHndlr.sendError(res, error)
        }
      });
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });
// Export modules
module.exports = userRouter;

