//= ========================= Load Modules Start =======================

const resHndlr = require("_/resHandler");
const { ROUTE_PREFIX } = require("_/constants");
const middleware = require("_/middleware/authentication");
const {ROLES} = require("_/constants");
const userRouter = require("_/routes/userRoute");
const customerRouter = require("_/routes/customerRoute");
const masterDataRoute = require("_/routes/masterDataRoute");
const unitRouter = require('_/routes/unitRoute');

//= ========================= Export Module Start ==============================

module.exports = function (app) {
  app.use(`${ROUTE_PREFIX}user`, userRouter);
  app.use(`${ROUTE_PREFIX}master`, masterDataRoute);
  // app.use(`${ROUTE_PREFIX}customer`,middleware.authenticateAndAuthorizeToken([ ROLES.SALES_MANAGER ]), customerRouter)
  app.use(`${ROUTE_PREFIX}customer`, middleware.authenticateAndAuthorizeToken(), customerRouter)
  app.use(`${ROUTE_PREFIX}unit`, middleware.authenticateAndAuthorizeToken() , unitRouter);

  app.use(resHndlr.hndlError);
};

//= ========================= Export Module End ===============================
