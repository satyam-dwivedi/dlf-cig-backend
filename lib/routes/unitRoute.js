'use strict'
const unitRouter = require("express").Router();
const resHndlr = require("_/resHandler");
const unitController = require("_/controllers/unitController");

/**
 *  Allot the unit to a customer 
 */
 unitRouter.route("/allot")
  .post(async (req, res, next) => {
    try {
      if(req && req.user) req.body.userId = req.body.created_by = req.body.updated_by = req.user.id;
      const result = await unitController.allotUnit(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });


  module.exports = unitRouter;