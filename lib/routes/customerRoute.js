'use strict'
const customerRouter = require("express").Router();
const resHndlr = require("_/resHandler");
const customerController = require("_/controllers/customerController");

/**
 * Customer record creation 
 */
 customerRouter.route("/")
  .post(async (req, res, next) => {
    try {
      if(req && req.user) req.body.userId = req.body.created_by = req.body.updated_by = req.user.id;
      const result = await customerController.saveCustomerData(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

/**
 * get customer record
 */
 customerRouter.route("/")
  .get(async (req, res, next) => {
    try {
      const result = await customerController.getCustomerList(req.user.id);
      resHndlr.sendSuccess(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });


/**
 * get alloted unit wise record
 */
 customerRouter.route("/alloted-units")
 .get(async (req, res, next) => {
   try {
     const result = await customerController.getAllotedUnits(req.user.id);
     resHndlr.sendSuccess(res, result);
   }
   catch (error) {
     resHndlr.sendError(res, error);
   };
 });


/**
 *export unit with customer preferences
 */
 customerRouter.route("/dashboard")
 .get(async (req, res, next) => {
   try {
     const result = await customerController.getPlotDashboard(req.query);
     resHndlr.sendSuccess(res, result);
   }
   catch (error) {
     resHndlr.sendError(res, error);
   };
 });


/**
 * get individual customer's record
 */
 customerRouter.route("/:id")
 .get(async (req, res, next) => {
   try {
     const result = await customerController.getCustomerData(req.params.id);
     resHndlr.sendSuccess(res, result);
   }
   catch (error) {
     resHndlr.sendError(res, error);
   };
 });

/**
 *update customer record and preferences
 */
 customerRouter.route("/:id")
  .put(async (req, res, next) => {
    try {
      req.body.id = req.params.id;
      const result = await customerController.updateCustomerData(req.body);
      resHndlr.sendSuccessWithMsg(res, result);
    }
    catch (error) {
      resHndlr.sendError(res, error);
    };
  });

// Export modules
module.exports = customerRouter; 
