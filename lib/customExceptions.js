//= ========================= Load Modules Start ===========================

//= ========================= Load Internal Module =========================

const Exception = require("_/Exception");
const constants = require("_/constants");
const appUtils = require("_/appUtils");

//= ========================= Load Modules End =============================

//= ========================= Export Module Start ==========================

module.exports = {
  intrnlSrvrErr(err,errMsg) {
    return new Exception(1,errMsg || "Please try after sometime", err);
  },
  badRequestError(errMsg,err) {
    return new Exception(2,errMsg||"bad request",err);
  },
  unAuthenticatedAccess(errMsg,err) {
    return new Exception(3,errMsg || "Unauthorized access", err);
  },
  conflictError(errMsg,err){
    return new Exception(4, errMsg || "duplicate entry not allowed", err);
  },
};

//= ========================= Export Module End ===========================
