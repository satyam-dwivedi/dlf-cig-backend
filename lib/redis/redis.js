//= ========================= Load Modules Start ==========================

//= ========================= Load External Modules ==========================

const Promise = require("bluebird");
const dbConfig = require("_/config/dbConfig");
const redisClient = dbConfig.client;
const redis = Promise.promisifyAll(redisClient);

//= ========================= Load Internal Modules ==========================
//= ========================= Load Modules End ==========================


const setValue = async function (key, value) {
  try {
    return await redisClient.setAsync(key, value);
  } catch (e) {
    throw e;
  }
  // return redisClient.setAsync(key, value)
  //   .then(result => result)
  //   .catch((err) => {
  //     throw err;
  //   });
};

const getValue = async function (key) {
  try {
    return await redisClient.getAsync(key);
  } catch (e) {
    throw e;
  }
  // return redisClient.getAsync(key)
  //   .then(result => result)
  //   .catch((err) => {
  //     throw err;
  //   });
};

const deleteKey = async function (key) {
  try {
    return await redisClient.delAsync(key);
  } catch (e) {
    throw e;
  }
  // return redisClient.delAsync(key)
  //   .then(result => result)
  //   .catch((err) => {
  //     throw err;
  //   });
};

const setJWTToken = function (params) {
  // const key = params.userToken;
  // const value = { jwtToken: params.jwtToken, id: params.id };
  // const cacheToken = JSON.stringify(value);
  // const saveTKnPrms = setValue(key, cacheToken);
  const userTokenKey = params.jwtToken;
  const userToken = params.id;
  const saveUsrTknPrms = setValue(userTokenKey, userToken);
  return saveUsrTknPrms;
  // return Promise.join(saveUsrTknPrms)
  //   .then((result) => {
  //     return result;
  //   })
  //   .catch((err) => {
  //     throw err;
  //   });
};

module.exports = {
  setJWTToken,
  setValue,
  getValue,
  deleteKey,
};

