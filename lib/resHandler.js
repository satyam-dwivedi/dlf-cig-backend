//= ========================= Load Modules Start ===========================

//= ========================= Load Internal Module=========================

// Load exceptions
const appConst = require("_/constants");
const excep = require("_/customExceptions");
const APIResponse = require("_/APIResponse");

//= ========================= Load Modules End =============================

function sendResponse(res, rslt) {
  if (rslt && rslt.error && (rslt.error.errorCode === 1)) {
    return res.send(500, rslt);
  }
  else if (rslt && rslt.error && (rslt.error.errorCode === 2)) {
    return res.send(400, rslt);
  }
  else if (rslt && rslt.error && (rslt.error.errorCode === 3)) {
    return res.send(401, rslt);
  }
  else if (rslt && rslt.error && (rslt.error.errorCode === 4)) {
    return res.send(409, rslt);
  }
  // send status code 200
  return res.send(rslt);
}


function sendError(res, err) {
  // if error doesn't has sc than it is an unhandled error,
  // log error, and throw intrnl server error
  if (!err.errorCode) {
    console.log("unhandled error = ", err);
    err = excep.intrnlSrvrErr(err);
  }
  const result = new APIResponse(appConst.STATUS_CODE.ERROR, err);
  sendResponse(res, result);
}
function hndlError(err, req, res, next) {
  // unhandled error
  sendError(res, err);
}

function sendSuccessWithMsg(res, msg) {
  const rslt = { message: msg };
  const result = new APIResponse(appConst.STATUS_CODE.SUCCESS, rslt);
  sendResponse(res, result);
}

function sendSuccess(res, rslt = {}, message) {
  if (message) {
    rslt.message = message;
  }
  const result = new APIResponse(appConst.STATUS_CODE.SUCCESS, rslt);
  sendResponse(res, result);
}


//= ========================= Exposed Action Start ==========================

module.exports = {
  hndlError, sendError, sendSuccess, sendSuccessWithMsg,
};

//= ========================= Exposed Action End ==========================


function sendFile(res, options, data) {
  // res.writeHead(200, options);
  res.set(options);
  return res.send(data);
}
