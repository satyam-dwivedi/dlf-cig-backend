//const constants = require("_/constants");
// const availableLanguages = ['es', 'en'];
const messages = {
    recordInserted: 'Record created successfully!',
    statusUpdated: 'Status has been updated',
    parameterMissing: 'Required parameters are missing',
    updateSuccessful: 'Record updated successfully!',
    mobileNoExists: 'Mobile no already exists',
    otpSent: 'Otp has been sent on your email',
    userNotExist: 'User does not exist',
    eoiExists: 'EOI for this customer has already been recorded',
    unitAlloted: 'Unit alloted successfully'
}

module.exports = {
    messages
}
