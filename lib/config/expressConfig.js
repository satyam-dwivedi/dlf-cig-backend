//= ==============================Load Modules Start======================== //

const express = require("express");
const bodyParser = require("body-parser");// parses information from POST
const appUtils = require("_/util");
const path = require("path");
const fs = require("fs");
const cors = require('cors')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('_/swagger.json');

//== ============================== End ==================================== //

module.exports = function (app, env) {
  // view engine setup
  app.set('views', path.join(__dirname, '../../views'));
  app.use(express.static('public'));
  // parses application/json bodies
  app.use(bodyParser.json());
  // use queryString lib to parse urlencoded bodies
  // parses application/x-www-form-urlencoded bodies
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cors());
  // app.use((req, res, next) => {
  //   appUtils.setHeadersForCrossDomainIssues(res);
  //   next();
  // });
  app.use('/apiDocs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  // app.use("/apiDocs", express.static(`${app.locals.rootDir}/public/dist`));
  var dest = path.join('./upload');
  function makeDirectories() {
    if (!fs.existsSync(dest)) {
      fs.mkdirSync(dest);
    }
    return dest;
  }
  app.use('/dlf-fms/api/v0/upload/',express.static(makeDirectories()));

};
