//= ============================ Load Modules Start ============================

const env = require("_/config/env");
const expressConfig = require("_/config/expressConfig");
const configDB = require("_/config/dbConfig");

//= ============================ Load Modules End ==============================

// Export config module
module.exports = {
  env,
  expressConfig,
  configDB,

};
