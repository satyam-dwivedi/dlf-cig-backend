
//= ================================== Load Modules start ===================================

//= ================================== Load external modules=================================

const logger = require("_/logger").logger;
const config = require("_/config/env");
const redis = require("redis");
const Sequelize = require("sequelize");
//= ================================== Load Modules end =====================================

const client = redis.createClient(config.redisDb.port, config.redisDb.host);

const connectRedis = function (env, callback) {
  client.select(env.redisDb.redisDBIndex, (err, res) => {
    if (err) {
      callback(err, null);
    } else {
      callback(null, res);
    }
  });
  client.on("error", (err) => {
    logger.error(`Error ${err}`);
    callback(err, null);
  });
};

let sequelize = new Sequelize(config.db.database,
  config.db.user, config.db.password, {
  host: config.db.host,
  port: config.db.port,
  dialect: config.db.dialect,
  dialectOptions: config.db.dialectOptions,
  // query: config.db.query,
  timezone: config.db.timezone, //for writing to database
});


const initConnection = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connected to mysql db");
  } catch (e) {
    throw e;
  }
};

module.exports = {
  connectRedis,
  client,
  sequelize,
  initConnection,
};