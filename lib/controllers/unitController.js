const unitDao = require("_/dao/unitDao");
const exceptions = require("_/customExceptions");
const messages  = require("_/messages").messages;
const customExceptions = require("_/customExceptions");

const unitController = {};

unitController.allotUnit = async (params) => {
    try{
        if(!params.unit_id || !params.customer_id) throw customExceptions.badRequestError('Invalid request');
        await unitDao.allotUnit(params); 
        return messages.unitAlloted;
    }catch(e){
        throw e;
    }
}

module.exports = unitController;

