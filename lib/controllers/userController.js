const userDao = require("_/dao/userDao");
const appUtils = require("_/appUtils");
const path = require("path");
const emailService = require("_/email/mailService");
const exceptions = require("_/customExceptions");
const jwtHandler = require("_/jwtHandler");
const redisClient = require("_/redis/redis");
const messages  = require("_/messages").messages;
const {commonConstants,EMAIL_SUBJECTS, MAIL_TEMPLATE_PATH} = require("_/constants");
const  mailService  = require('_/email/mailService')
const EOI_URL = require('_/config').env.EOI_URL;
const fs = require('fs'); 


const userController = {};

userController.sendLoginOtp = async (params) => {
    try {
        let user = await userDao.getUserDetails(params);
        params.otp = appUtils.getRandomOtp();
        params.otp_sent_time = Date.now();
        const data = await userDao.updateOtp(params);

        if(data[0] === 0){
            throw exceptions.badRequestError(messages.userNotExist); 
        }

        const html = (fs.readFileSync(MAIL_TEMPLATE_PATH.LOGIN_OTP))
                        .toString()
                        .replace(/-eoi_url-/g, EOI_URL)
                        .replace("-code-", params.otp)
                        .replace("-name-", user.name);

        mailService.send({
            subject: EMAIL_SUBJECTS.LOGIN_OTP,
            email: user.email,
            html: html,
            otp: params.otp,
            name: user.name,
        });

        
        return messages.otpSent;
    } catch (error) {
       throw error; 
    }
}


userController.login = async (params) => {
    try {
        let user = await userDao.getUserDetails(params);
        if (!user)
            throw exceptions.badRequestError(messages.invalidMail);
        diffMs = Date.now() -  Date.parse(user.otp_sent_time);
        var diffMins = Math.floor(diffMs / 60000); // minutes
        if (diffMins >= commonConstants.OTP_EXPIRATION_TIME)
            throw exceptions.badRequestError(messages.otpExpired);
        let data = await userDao.login(params);
        if (!data)
            throw exceptions.badRequestError(messages.invalidOtp);
        else {
            data = data.get({ plain: true });
            let token = await jwtHandler.generateToken({ id: data.id});
            data.token = token;
        }
        return data;
    } catch (error) {
        throw error;
    }
}

// This function is used to logout user / partner
userController.logout = async (params) => {
    try {
        const resp = await redisClient.getValue(params.token);
        console.log(resp, "response");
        if (resp)
            await redisClient.deleteKey(params.token);
        return messages.loggedOut;
    } catch (e) {
        throw e;
    }
};

userController.getUserDetails = async (params) => {
    try {
        let data = await userDao.getUserDetails(params);
        if(!data)
        throw exceptions.badRequestError("user doesn't exist");
        return data;
    } catch (error) {
        throw error;
    }
}

userController.addUser = async (params) => {
    try {
        let data = await userDao.addUser(params);
        if(!data)
        throw exceptions.badRequestError("user doesn't exist");
        return data;
    } catch (error) {
        throw error;
    }
}

userController.downloadDocument =  ( params ,res) => {
    try {
        let filePath = path.join(__dirname,'../../',params.file_path);
        console.log(params,"params",filePath,"filePath");
        return filePath;
    } catch (error) {
        throw error;  
    }
}

module.exports = userController;
