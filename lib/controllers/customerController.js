const customerDao = require("_/dao/customerDao");
const exceptions = require("_/customExceptions");
const messages  = require("_/messages").messages;


const customerController = {};

customerController.saveCustomerData = async (params) => {

    //since 'value_in_crs' and optional, check whether
    //it is a valid Number
    if(params.value_in_crs && parseFloat(params.value_in_crs) != NaN){
        params.value_in_crs = parseFloat(params.value_in_crs);
    }else {
        delete params.value_in_crs;
    }

    if(!params.first_preference) throw exceptions.badRequestError(messages.parameterMissing);

    //By default set first preference as alloted unit for this customer
    params.alloted_unit = params.first_preference;

    try{
        await customerDao.saveCustomerData(params); 
        return messages.recordInserted;
    }catch(e){
        throw exceptions.conflictError(messages.eoiExists);
    }
}

customerController.getCustomerList = async (userId) => {
        return await customerDao.getCustomerList(userId);
}

customerController.getAllotedUnits = async (userId) => {
    return await customerDao.getAllotedUnits(userId);
}

customerController.getCustomerData = async (customerId) => {
    return await customerDao.getCustomerData(customerId);
}

customerController.updateCustomerData = async (params) => {
    let data =  await customerDao.updateCustomerData(params); 
    if(!data || !data[0])
    throw exceptions.badRequestError(messages.mobileNoExists);
    else
    return messages.updateSuccessful;
}

customerController.getPlotDashboard = async (params) => {
    return await customerDao.getPlotDashboard(params);
}

module.exports = customerController;