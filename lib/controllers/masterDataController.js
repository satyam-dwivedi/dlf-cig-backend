const masterDataDao = require("_/dao/masterDataDao");
const userDao = require("_/dao/userDao");
const appUtils = require("_/appUtils");
const path = require("path");
const exceptions = require("_/customExceptions");
const messages = require("_/messages").messages;
const middleware = require("_/middleware");
const { TENANT_USER_TYPE, CONSULTANT_TYPE, ROLES } = require("_/constants");

const masterDataController = {};

/*
*get master data of different tables
*/
masterDataController.getMasterData = async (params) => {
    console.log(params, "params");
    switch (params.reqtype) {
        case 'employee':
            return await masterDataDao.getDlfEmployees(params);
        case 'project':
            return await masterDataDao.getProjects(params);
        case 'user':
            return await masterDataDao.getUsers(params);
        case 'project':
            return await masterDataDao.getProjects(params);
        case 'tower':
            return await masterDataDao.getTower(params);
        case 'floor':
            return await masterDataDao.getFloor(params);
        case 'property':
            return await masterDataDao.getProperty(params);
        case 'unit':
            return await masterDataDao.getUnit(params);
        default:
            console.log("Default");
            break;
    }
}

/*
*create master data of different tables
*/
masterDataController.createMasterData = async (params) => {
    try {
        switch (params.reqtype) {
            case 'employee':
                await masterDataDao.createEmployee(params);
                break;
            case 'project':
                return await masterDataDao.addProject(params);
            //  break;
            case 'tower':
                await masterDataDao.addTower(params);
                break;
            case 'floor':
                return await masterDataDao.addFloor(params);
            case 'property':
                await masterDataDao.addProperty(params);
                break;
            default:
                console.log("Default");
                break;
        }
        return messages.recordInserted;
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}", params.reqtype));
        throw error;
    }
}

/*
*update master data of different tables
*/
masterDataController.updateMasterData = async (params) => {
    try {
        console.log(params, "params");
        switch (params.reqtype) {
            case 'employee':
                return await masterDataDao.updateEmployee(params);
            case 'team':
            case 'project':
                return await masterDataDao.updateProject(params);
            case 'tower':
                return await masterDataDao.updateTower(params);
            case 'floor':
                return await masterDataDao.updateFloor(params);
            case 'property':
                return await masterDataDao.updateProperty(params);
            case 'user':
                return await masterDataDao.updateSystemUser(params);
            default:
                console.log("Default");
                break;
        }
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}", params.reqtype));
        throw error;
    }

}

/*
*activate/deactivate different masters data like consultants
*/
masterDataController.updateStatus = async (params) => {
    let updatedStatus = await masterDataDao.updateStatus(params);
    console.log(updatedStatus, "updstatas");
    if (updatedStatus && updatedStatus[0] > 0)
        return messages.statusUpdated;
    else
        throw exceptions.badRequestError(messages.badRequest);

}


//single doc upload
masterDataController.uploadDocument = async (params) => {
    try {
        return params;
    } catch (error) {
        throw error;
    }
}

/*
*create system users of different roles
*/
masterDataController.createSystemUser = async (params) => {
    try {
        console.log(params, "params");
        if (params.roles_id == ROLES.ADMIN 
            || params.roles_id == ROLES.FMS  
            || params.roles_id == ROLES.CRM
            || (params.roles_id == ROLES.CONSULTANT && params.type == CONSULTANT_TYPE.INTERNAL)
            ) {

            let employee = await masterDataDao.getDlfEmployees({ employee_id: params.reference_id });
            console.log(employee, "empData");
            params.name = employee[0].name;
            params.email = employee[0].email;
            await masterDataDao.createSystemUser(params);
            return messages.userCreated;
        }
        if (params.roles_id == ROLES.CONSULTANT && params.type == CONSULTANT_TYPE.EXTERNAL) {
            
            await masterDataDao.createSystemUser(params);
            return messages.userCreated;

        }
    
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}", params.reqtype));
        throw error;
    }
}


module.exports = masterDataController;
