//= ========================= Load Modules Start ===========================

//= ========================= Load External Module =========================

const _ = require("lodash");

//= ========================= Load Internal Module =========================

const appConstants = require("_/constants");
const path = require("path");
const fs = require("fs");
const randomstring = require("random-string");
const moment = require("moment");
const config = require("_/config").env;
const momentTimezone = require("moment-timezone");
const exceptions = require('_/customExceptions');
const bcrypt = require('bcryptjs');
const messages = require("_/messages").messages;
const {commonConstants} = require("_/constants");
//= ========================= Load Modules End =============================


//= ========================= Export Module Start ===========================

/**
 * return user home
 * @returns {*}
 */
function getUserHome() {
  return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
}

function getNodeEnv() {
  return process.env.NODE_ENV || "local";
}

function getWebUrl() {
  return config.webBaseUrl;
}

function isProdEnv() {
  const env = getNodeEnv();
  return _.includes(["prod", "production"], env);
}

/**
 * returns if email is valid or not
 * @returns {boolean}
 */
function isValidEmail(email) {
  const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return new RegExp(pattern).test(email);
}

/**
 * returns if zipCode is valid or not (for US only)
 * @returns {boolean}
 */
function createHashSHA256(pass) {
  return sha256(pass);
}

/**
 * returns random number for password
 * @returns {string}
 */
const getRandomPassword = function () {
  return getSHA256(Math.floor((Math.random() * 1000000000000) + 1));
};

let getSHA256 = function (val) {
  return sha256(`${val}password`);
};

function setHeadersForCrossDomainIssues(res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Origin,X-Requested-With,Content-Type,Accept,Authorization, x-custom-token , X-XSRF-TOKEN, applang");
  res.header("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE,OPTIONS");
  return res;
}

function getEmailVerificationLink(token) {
  return `${getWebUrl()}verify/${token}`;
}

function ForgotPasswordLink(token) {
  return `${getWebUrl()}reset/${token}`;
}

function GET_PRJCT_HME_DIR() {
  return path.join(__dirname, "..");
}

/*
 @ calculate the milliseconds
 */
function currentUnixTimeStamp(params) {
  if (params) {
    return Math.floor(new Date(params));
  }
  return Math.floor(Date.now());
}


function createDirectory(dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

/**
 *
 * @returns {string}
 * get random 6 digit number
 * FIX ME: remove hard codeing
 * @private
 */
const getRandomOtp = () => {
  // Generate Random Number
  const otp = randomstring({
    length: 6,
    numeric: true,
    letters: false,
    special: false,
    exclude: ["0"],
  });
  console.log(`otp ${JSON.stringify(otp)}`);
  return otp;
};

/* generates referral code
*/
const referralCodeGen = (params) => {
  let code1 =  alphaNumericGenerator(4);
  let code2 = alphaNumericGenerator(4);
  let finalCode = code1 +params.uid+code2;
  return finalCode;
};

const getLast2WeeksTimestamp = () => {
  const timestamp = Number(moment().subtract(14, "day")) / 1000;
  return timestamp;
};

/* converts miles to km
*/
const convertMilesToKm = (miles) => {
  const value = parseInt(miles) / 0.62137;
  return value;
};

const removeSpecialCharacter = string => string.replace(/[^0-9]/g, "");

const unixTimeFormatter = date => `${date.getFullYear()}-${(`0${date.getMonth() + 1}`).slice(-2)}-${(`0${date.getDate()}`).slice(-2)}`;


const getNextDates = (unixtime, daysToAdd) => {
  const aryDates = [];
  const startDate = new Date(unixtime * 1000);

  for (let i = 0; i < daysToAdd; i++) {
    const currentDate = new Date();
    currentDate.setDate(startDate.getDate() + i);
    aryDates.push(unixTimeFormatter(currentDate));
  }
  console.log(`array ${JSON.stringify(aryDates)}`);
  return aryDates.map(date => `'${date}'`).join(",");
};

const sanitizeString = string => _.trim(_.toLower(string));
const removeSpaceString = string => _.trim(string);
const getStartOfDayTimestamp = date => Number(moment(date).startOf("day")) / 1000;
const getEndOfDayTimestamp = date => Number(moment(date).endOf("day")) / 1000;
const getNext7DayTimestamp = (date) => {
  const timestamp = moment(date).add(9, "day").format("YYYY-MM-DD");
  return timestamp;
};
const getNextDayTimeStamp = (date, day) => {
  const timestamp = Number(moment(date).add(day, "day"));
  return timestamp;
};
const getUTCDate = params => momentTimezone.tz(params.date, "YYYY-MM-DD", params.time_zone).utc().format("YYYY-MM-DD");
const getStartDayTimestamp = (params) => {
  const date = momentTimezone.tz(params.date, "YYYY-MM-DD", params.time_zone);
  return Number(moment(date).startOf("day").utc()) / 1000;
};

const getEndDayTimestamp = (params) => {
  const date = momentTimezone.tz(params.date, "YYYY-MM-DD", params.time_zone);
  return Number(moment(date).endOf("day").utc()) / 1000;
};

const getDateFromTimestamp = selectedDate => moment(selectedDate).format("YYYY-MM-DD");

function replaceAll(string, replacementArray) {
  return string.replace(/({{\d}})/g, j => replacementArray[j.replace(/{{/, "").replace(/}}/, "")]);
}

const getDateFromTimestampByZone = (params) => {
  const time = moment(params.date).utc().add(-2, "day").format("YYYY-MM-DD");
  const date = momentTimezone.tz(time, "YYYY-MM-DD", params.time_zone);
  return moment(date).format("YYYY-MM-DD");
};

const getUTCDateByTimezone = params => momentTimezone.tz(params.date, "YYYY-MM-DD", params.time_zone).format("YYYY-MM-DD");

const startCharacterInUpperCase = str => _.startCase(_.toLower(str));

const getFirstCharacter = (str) => {
  const lastName = str.toUpperCase();
  return `${lastName.substring(0, 1)}.`;
};

// Remove special character from string
const removeSpecialCharacterAndSpace = (string) => {
  let newString = string.replace(/[^a-zA-Z 0-9 ]/g, "");
  newString = newString.replace(/\s/g, "_");
  newString = sanitizeString(newString);
  return newString;
};

const getDateFromTimeStamp = timestamp => moment(Number(timestamp)).format("DD/MM/YY");

const passwordLength = (password) => {
  const pwdLength = password.length;
  let length = false;
  if (pwdLength > appConstants.MIN_PASSWORD_LENGTH)
    length = true;
  return length;
}

const daysInThisMonth = () => {
  var now = new Date();
  return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
}

// This function is used to check length
const checkAmountLength = (value) => {
  return (value.toString()).length > appConstants.REQUIRED_LENGTH.MAX_AMOUNT_LENGTH
    ? true : false;
};

// This function is used to check length
const checkPhoneLength = (value) => {
  return (value.toString()).length > appConstants.REQUIRED_LENGTH.MAX_PHONE_LENGTH
    ? true : false;
};

const getCurrentUTCDateTimestamp = () => {
  return Number(moment().utc().startOf("day"));
};

const removeSpecialCharacterFromString = (string) => {
  return string.replace(/[^\w\s]/gi, '');
};

// This function is used to remove extra space from string
const removeExtraSpace = (string) => {
  return string.replace(/\s+/g, ' ').trim();
};

//This function is used to check the image size
const checkImageSize = (file) => {
  const stats = fs.statSync(file.path)
  const fileSizeInBytes = stats.size
  const fileSizeInMegabytes = fileSizeInBytes / 1000000.0
  if (fileSizeInMegabytes > 20)
    return false;
  else
    return true;
}

const checkLengthOfNumber = (value) => {
  if (value.toString().length === 13) {
    return true;
  }
  return false;
};

const alphaNumericGenerator = (strLength) => {
  const chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
  let referralcode = "";
  for (let i = 0; i < strLength; i++) {
    const rnum = Math.floor(Math.random() * chars.length);
    referralcode += chars.substring(rnum, rnum + 1);
  }
  return referralcode;
};

const getRandomInvoiceNumber = (prefix) => {
  const randomNumber = alphaNumericGenerator(8);
  return prefix + randomNumber;
};

const getPreviousDaysTimeStamp = (params) => {
  let data = moment().subtract(params, 'days').startOf('day').unix()
  return (data * 1000)
}

const getCurrentMonth = () => {
  let date = new Date();
  return (date.getMonth() + 1)
}

const getTimeFromDate = timestamp => moment(Number(timestamp)).format("hh:mm:ss");

function encryptPassword(pass) {
  if (pass) {
    var hash = bcrypt.hashSync(pass, 10);
    return hash;
  }
}

function decryptPassword(pass, hash) {
  if (pass && hash) {
    return bcrypt.compareSync(pass, hash);
  }
  else {
    return false;
  }
}

const calculateDaysBetweenTimeStamps = function (timestamp1, timestamp2) {
  var difference = timestamp1 - timestamp2;
  var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
  return (daysDifference+1);
}

const checkImageMimeType = (file) => {
  let mimeType = file.mimetype;
  let data = mimeType.split("/")
  mimeType = data[1];
  mimeType = mimeType.toLowerCase();
  if (mimeType === "jpg" || mimeType === "jpeg" || mimeType === "png" || mimeType === "bmp" || mimeType === "tiff")
    return true;
  else
    return false;
}

function getHoursDifference(date1, date2) {
  return (Math.abs(date1 - date2) / 36e5);
}

const deleteSensitiveFields = (params) => {
  delete params.password;
  delete params.otp;
};

// Export app language messages based on user provided language
const getAppLanguageMessages = (appLanguage) => {
  return appLanguage === appConstants.APP_LANGUAGE.ES ? messages.es : messages.en;
};
//to validate otp
const isValidOtp = (otp) =>{
  if (isNaN(otp) || otp.length != 3) {
    return false;
  }
  return true;
}

//to get start and end day utc time stamp
const getStartAndEndOfDayUnixTimestamp=()=>{
const interval = 1000 * 60 * 60 * 24; // 24 hours in milliseconds
let startOfDay = Math.floor(Date.now() / interval) * interval;
let endOfDay = startOfDay + interval - 1; // 23:59:59:9999
return{start:startOfDay,end:endOfDay}
}

const fileValidation = (req) => {
  let uploadedFiles = req && req.files;
  if (uploadedFiles) {
      uploadedFiles.map((file) => {
          console.log(file,"eachFile");
          if (file.size > commonConstants.MAX_FILE_SIZE)
              throw exceptions.badRequestError(` file size is larger than 50 MB`);

      })
  }
  console.log("--------------------------------------------validation Passed---------------------------");
}

module.exports = {
  getNodeEnv,
  setHeadersForCrossDomainIssues,
  isValidEmail,
  getEmailVerificationLink,
  ForgotPasswordLink,
  currentUnixTimeStamp,
  GET_PRJCT_HME_DIR,
  getRandomOtp,
  referralCodeGen,
  getLast2WeeksTimestamp,
  convertMilesToKm,
  removeSpecialCharacter,
  unixTimeFormatter,
  getNextDates,
  sanitizeString,
  removeSpaceString,
  getStartOfDayTimestamp,
  getEndOfDayTimestamp,
  getNext7DayTimestamp,
  getNextDayTimeStamp,
  getUTCDate,
  getStartDayTimestamp,
  getEndDayTimestamp,
  getDateFromTimestamp,
  getDateFromTimestampByZone,
  getUTCDateByTimezone,
  startCharacterInUpperCase,
  getFirstCharacter,
  removeSpecialCharacterAndSpace,
  getDateFromTimeStamp,
  passwordLength,
  daysInThisMonth,
  checkAmountLength,
  checkPhoneLength,
  getCurrentUTCDateTimestamp,
  removeSpecialCharacterFromString,
  removeExtraSpace,
  checkImageSize,
  checkLengthOfNumber,
  getRandomInvoiceNumber,
  getPreviousDaysTimeStamp,
  getCurrentMonth,
  getTimeFromDate,
  encryptPassword,
  decryptPassword,
  calculateDaysBetweenTimeStamps,
  checkImageMimeType,
  getHoursDifference,
  deleteSensitiveFields,
  getAppLanguageMessages,
  isValidOtp,
  getStartAndEndOfDayUnixTimestamp,
  fileValidation
};

//========================== Export Module End ===========================
