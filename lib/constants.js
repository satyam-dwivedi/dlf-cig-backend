const path = require("path");

const commonConstants = {
  OTP_EXPIRATION_TIME: 5,
  MAX_FILE_SIZE: 50*1024*1024,
}

const STATUS_CODE = {
  ERROR: 0,
  SUCCESS: 1,
  INVALID_TOKEN:401,
};

const DB_ERROR_CODES = {
  DUPLICATE: "23505",
  UNIQUE_EMAIL_CONSTRAINT: "users_email_key",
  UNIQUE_PHONE_CONSTRAINT: "users_phone_key",
};

const ROLES = {
  SALES_MANAGER: 1,
  ADMIN: 2
};


const DEVICE_TYPE = {
  ANDROID: 1,
  iOS: 2,
  WEB: 3,
};

const PAGINATION = {
  START_INDEX: 0,
  COUNT: 20,
};

const STATUS = {
  DELETED: 0,
  ACTIVE: 1,
  INACTIVE: 2
}

const UPLOAD_STATUS = {
  NOT_UPLOADED: 1,
  UPLOADED: 2,
}

const PROJECT_TYPE = {
  INDEPENDENT_FLOORS: 1,
  HIGHRISE: 2
}

const EMAIL_SUBJECTS = {
  LOGIN_OTP: 'SALES EOI | Login OTP',
}

const MAIL_TEMPLATE_PATH = {
  LOGIN_OTP: path.join(__dirname, '../views/loginCode.html'),
}

const EOI_URL = 'saleseoiqa.dlf.in'
//= ========================= Export Module Start ========================== //

module.exports = {
  commonConstants,
  STATUS_CODE,
  ROUTE_PREFIX: "/dlf-cig/api/v0/",
  ROLES,
  PAGINATION,
  DEVICE_TYPE,
  DB_ERROR_CODES,
  STATUS,
  UPLOAD_STATUS,
  PROJECT_TYPE,
  MAIL_TEMPLATE_PATH,
  EMAIL_SUBJECTS
};


//= ========================= Export Module END ===========================