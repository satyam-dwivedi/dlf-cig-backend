
const exceptions = require("_/customExceptions");
const { STATUS, ROLES, CONSULTANT_TYPE,APPROVAL_STATUS } = require("_/constants");
const messages = require("_/messages").messages;

const {Op,QueryTypes} = require("sequelize");
const Models = require("_/model");
const sequelize = require("_/config/dbConfig").sequelize;

const masterDataDao = {};

//---------------------------- *MASTER GET OPERATIONS* ---------------------------------


/*
*get list of dlfEmployees
*/
masterDataDao.getDlfEmployees = async (params) => {
    try {
        let condition = {
            where:
            {
                status: STATUS.ACTIVE
            },
            order: [
                ['status', 'ASC'],
                ['updated_at', 'DESC']
            ],
            raw: true
        };
        if(params.requiredData == 'all')
            condition.where.status = {[Op.ne]: STATUS.DELETED};
        if(params.employee_id)
            condition.where.id = params.employee_id;    
        if (params.roles_id && !(params.roles_id == ROLES.CONSULTANT && params.type == CONSULTANT_TYPE.EXTERNAL)) {
            condition.where.email = {
                [Op.notIn]: sequelize.literal(`(select email from users where roles_id = ${params.roles_id})`)
            };
        }
        return Models.Employees.findAll(condition);
    } catch (error) {
        throw error;
    }
}

/*
*get list of projects
*/
masterDataDao.getProjects = async (params) => {
    try {
        let options = {
            type: QueryTypes.SELECT,
            replacements: {
                projectId: params.project_id,
                user_id: params.userId
            }
        }
        let where = ``;
        console.log(params, "parammsss");
        if (params.project_id) {
            where = ` where p.id = :projectId`;
            // options.replacements.projectId = params.project_id;
        }
        let query = `
            SELECT 
                p.id as project_id,
                p.name as project_name,
                p.scheme_code,
                p.type,
                p.manager_name,
                p.manager_email,
                (select count(*) from towers where towers.project_id = p.id) as tower_count
            FROM 
                projects p
            ${where}
            ORDER BY p.updated_at DESC;`;

        return await sequelize.query(query, options);
    } catch (error) {
        throw error;
    }
}


/*
*get all system users
*/
masterDataDao.getUsers = async (params) => {
    let rawQuery = `SELECT id,
        email,
        name,
        phone,
        roles_id,
        type user_type,
        project_id,
        (
        select
            group_concat(name) 
        from
            projects 
        where
            find_in_set(id, users.project_id)
        )
        project_name,
        status,
        created_by 
    FROM
        users 
    WHERE
        status != ${STATUS.DELETED} 
    ORDER BY
        status ASC,
        updated_at DESC`

    return await sequelize.query(rawQuery, { type: QueryTypes.SELECT })
}

masterDataDao.getFloor = async (params) => {

    let condition = true;

    if (params.project_id)
        condition = `${condition} and p.id = ${params.project_id}`;

    if (params.tower_id)
        condition = `${condition} and t.id = ${params.tower_id}`;

    if (params.floor_id)
        condition = `${condition} and f.id = ${params.floor_id}`;

    if (params.requiredData != 'All')
        condition = `${condition} AND f.status = ${STATUS.ACTIVE}`;

    let query = `
        SELECT 
            f.name,
            f.id,
            t.name as tower_name,
            t.id as tower_id,
            p.id as project_id,
            p.name as project_name
        FROM 
            floors f 
            INNER JOIN towers t on f.tower_id = t.id
            INNER JOIN projects p on t.project_id = p.id
        WHERE f.status = ${STATUS.ACTIVE} AND
            ${condition}
        ORDER BY 
            f.id;
    `
    return await sequelize.query(query, { type: QueryTypes.SELECT });
}

masterDataDao.getTower = async (params) => {
    let query = {
        attributes: ['id', 'project_id', 'name',
            [(sequelize.literal(`(SELECT count(*) FROM floors WHERE floors.tower_id = tower.id)`)), 'floor_count']
        ],
        where: {
        },
        order: [
            ['status', 'ASC'],
            ['created_at', 'ASC']
        ]
    }
    if (params.id) query.where.id = params.id;
    if (params.project_id) query.where.project_id = params.project_id;
    return await Models.Towers.findAll(query);
}

masterDataDao.getProperty = async (params) => {
    let condition = `true`;
    if (params.project_id) condition = `p.id = ${params.project_id}`
    if (params.tower_id) condition = `t.id = ${params.tower_id}`
    if (params.floor_id) condition = `f.id = ${params.floor_id}`;

    let query = `SELECT
                    prop.id,
                    prop.name,
                    p.id as project_id,
                    p.name as project_name,
                    t.id as tower_id,
                    t.name as tower_name
                FROM properties as prop
                    INNER JOIN floors as f ON f.id = prop.floor_id AND prop.status = ${STATUS.ACTIVE}
                    INNER JOIN towers as t ON t.id = f.tower_id AND f.status = ${STATUS.ACTIVE}
                    INNER JOIN projects as p ON p.id = t.project_id
                WHERE ${condition}
                ORDER BY prop.updated_at DESC;
    `
    return await sequelize.query(query, { type: QueryTypes.SELECT });
}

/**
 * 
 * @param {*} params 
 * @returns List of unit
 */
masterDataDao.getUnit = async (params) => {
    let condition = 'true';
    if(params.project) condition = `p.id = ${params.project}`
    let query = `SELECT
                    p.id as projectId,
                    t.id as towerId,
                    f.id as floorId,
                    unit.id as unitId,
                    unit.name as unitName
                FROM projects as p
                    INNER JOIN towers as t ON p.id = t.project_id AND t.status = ${STATUS.ACTIVE}
                    INNER JOIN floors f  ON f.tower_id = t.id AND f.status = ${STATUS.ACTIVE}
                    INNER JOIN properties unit  ON unit.floor_id = f.id AND unit.status = ${STATUS.ACTIVE}
                WHERE ${condition} AND unit.id NOT IN (
                    SELECT alloted_unit 
                    FROM customers 
                    WHERE status = ${STATUS.ACTIVE} AND sold
                )
                ORDER BY unit.name;

    `
    return await sequelize.query(query, { type: QueryTypes.SELECT });
}

//-------------------------------------------------------------------------------------------


//---------------------------- *MASTER CREATE OPERATIONS* -----------------------------------


//create employee
masterDataDao.createEmployee = async (params) => {
    try {
        params.status = STATUS.ACTIVE;
        return await Models.Employees.create(params);
    } catch (error) {
        throw error;
    }
}

masterDataDao.createSystemUser = async (params) => {
    // return await Models.Users.findOrCreate({
    //     where:
    //     {
    //         email: params.email,
    //         roles_id: params.roles_id
    //     },
    //     defaults: params,
    //     returning: true,
    //     raw: true,
    // })
    return await Models.Users.create(params)
}

//Project creation
masterDataDao.addProject = async (params) => {
    try {
        return await Models.Projects.create(params);
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}",params.reqtype));
        throw error;
    }
}

masterDataDao.addTower = async (params) => {
    try {
        console.log('--------------_TOWER------------', params);
        return await Models.Towers.create(params);
    } catch (error) {
        throw error;
    }

}

masterDataDao.addFloor = async (params) => {
    try {
        let floorsDetail = [];
        params.floors.forEach((floor) => {
            let floorData = {};
            floorData.name = floor.name;
            floorData.tower_id = floor.tower_id;
            floorData.created_by = params.created_by;
            floorData.updated_by = params.updated_by;
            floorsDetail.push(floorData);
        })
        console.log("-----------FLOOR!------------", floorsDetail);
        return Models.Floors.bulkCreate(floorsDetail);
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}",params.reqtype));
        throw error;
    }

}

masterDataDao.addProperty = async (params) => {
    try {
        console.log("_-----------------PROPERTY-----------------", params.property)
        return Models.Properties.bulkCreate(params.property);
    } catch (error) {
        if (error.original && error.original.errno && error.original.errno == 1062)
            throw exceptions.conflictError(messages.duplicateRecord.replace("{field}",params.reqtype));
        throw error;
    }

}

//--------------------------------------------------------------------------------------


//-------------------------- *MASTER UPDATE OPERATIONS* ---------------------------------



//upadate employee 
masterDataDao.updateEmployee = async (params) => {
    try {
        return await Models.Employees.update(params,
            {
                where: {
                    id: params.id,
                }
            });
    } catch (error) {
        throw error;
    }
}


//update project details
masterDataDao.updateProject = async (params) => {
    try {
        if(params.onlyManagerChange == true){
            return await Models.Projects.update({
                manager_name: params.manager_name,
                manager_email: params.manager_email
            },{
                where: {
                    id: params.project_id
                }
            })
        }
        let leaseCount = await masterDataDao.leasesCount({project_id: params.project_id});
        if( leaseCount && leaseCount[0].count > 0 )
            throw exceptions.badRequestError(messages.projectLeasesExists);
        let refDocsCount = await masterDataDao.referenceDocumentsCount({project_id: params.project_id});
        if( refDocsCount && refDocsCount[0].count > 0 )
            throw exceptions.badRequestError(messages.projectReferenceDocumentsExists);
        return await Models.Projects.update(params, {
            where: {
                id: params.project_id
            }
        });
    } catch (error) {
        throw error;
    }
}
// update tower 
masterDataDao.updateTower = async (params) => {
    try {
        console.log("tower upsdatejsddddd==========================", params);
        return await Models.Towers.update(params, {
            where: {
                id: params.id
            }
        });
    } catch (error) {
        throw error;
    }

}
//update floor details
masterDataDao.updateFloor = async (params) => {
    try {
        let floors = params.floors;
        for (let i = 0; i < floors.length; i++) {
            let isUpdate = await Models.Floors.update({
                name: floors[i].name
            }, {
                where: {
                    id: floors[i].id
                }
            });
            //logic to keep track of updated floor's name using status key
            if (isUpdate && isUpdate[0] == 1) floors[i].status = 1;
            else floors[i].status = 0;

            //reset isUpdate for next iteration
            isUpdate = [];
        }
        return floors
    } catch (error) {
        throw error;
    }
}

//update project property
masterDataDao.updateProperty = async (params) => {
    try {
        let leaseCount = await masterDataDao.leasesCount({project_id: params.project_id});
        if( leaseCount && leaseCount[0].count > 0 )
            throw exceptions.badRequestError(messages.projectLeasesExists);
        let refDocsCount = await masterDataDao.referenceDocumentsCount({project_id: params.project_id});
        if( refDocsCount && refDocsCount[0].count > 0 )
            throw exceptions.badRequestError(messages.projectReferenceDocumentsExists);
        return await Models.Properties.update({
            name: params.name,
            tower_id: params.tower_id
        }, {
            where: {
                id: params.id
            }
        });
    } catch (error) {
        throw error;
    }

}


/*
*activate/deactivate different masters data like consultants
*/
masterDataDao.updateStatus = async (params) => {
    let update = {
        status: params.status,
        updated_by: params.userId
    };
    let where = {
        id: params.id
    }
    console.log(params, "params");
    switch (params.reqtype) {

        case 'other_roles':
            return await Models.Users.update(update, { where });
        case 'employee':
            let t = await sequelize.transaction();
            try {
                let emailId = await Models.Employees.findOne({
                    attributes: ['email'],
                    where:{
                        id: params.id
                    }
                });
                await Models.Users.update(update, {
                    where: {
                        email: emailId.email
                    }, 
                    transaction: t
                });
                let updated = await Models.Employees.update(update, { where });
                t.commit();
                return updated;
            } catch (error) {
               t.rollback();
               throw error; 
            }   
        default:
            console.log("Default case");
            break;
    }
    console.log(params, "paramss");
}


masterDataDao.updateSystemUser = async (params) => {
    let userData = await Models.Users.findOne({
        where: {
            id: params.id,
            status: STATUS.ACTIVE
        },
        raw: true
    });
    if (!userData) throw exceptions.badRequestError("Invalid Request!");

    let dataToupdate = {
        updated_by: params.userId
    };

    if (userData.roles_id == ROLES.FMS && String(params.project_id).split(',').length > 0 ) {
        dataToupdate.project_id = params.project_id
    }else if (userData.roles_id == ROLES.CONSULTANT && userData.type == CONSULTANT_TYPE.EXTERNAL) {
        if (params.name) dataToupdate.name = params.name;
        if(params.email) dataToupdate.email = params.email;
    }else {
        throw exceptions.badRequestError("Invalid Request!");
    }

    console.log("DATATOUPLOAD",dataToupdate,"PARAMS",params)
    return await Models.Users.update(dataToupdate,{
        where: {
            id: params.id,
            status: STATUS.ACTIVE
        }
    }); 
}

module.exports = masterDataDao;