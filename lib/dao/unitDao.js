const sequelize = require("_/config/dbConfig").sequelize;
const { STATUS,ROLES } = require("_/constants");
const { Op,QueryTypes } = require("sequelize");
const Models  = require("_/model");
const unitDao = {};


unitDao.allotUnit = async (params) => {
    try{
        console.log('parammsssss----->>> from dao', params)
        return await Models.Customer.update({
            alloted_unit: params.unit_id,
            alloted_by: params.userId,
            alloted_at: Date.now(),
            sold: 1
        },{
            where: {
                id: params.customer_id
            }
        });
    } catch(e){
        console.log(e);
        throw e;
    }
}

module.exports = unitDao;