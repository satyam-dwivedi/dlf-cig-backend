
const sequelize = require("_/config/dbConfig").sequelize;
const { STATUS,ROLES, PROJECT_TYPE } = require("_/constants");
const { Op,QueryTypes } = require("sequelize");
const Models  = require("_/model");
const _ = require('lodash')
const customerDao = {};


customerDao.saveCustomerData = async (params) => {
    try{
        return await Models.Customer.create(params);
    } catch(e){
        console.log(e);
        throw e;
    }
}

customerDao.getCustomerList = async (userId) => {
    let adminAccount = [2,5,6,7,8,9,10,11];
    let where = '';

    if(adminAccount.indexOf(userId) === -1){
        where = `AND c.created_by = ${userId}`;
    }

    let query = `
    SELECT  c.id ,  
            c.name ,  
            c.phone ,  
            c.broker_name ,  
            c.sourcing ,  
            c.value_in_crs,  
            c.last_four_digit ,  
            c.closing_manager ,
            c.created_at , 
            concat(t1.name, '_' , first_unit.name) first_preference,
            concat(t2.name, '_' , second_unit.name) second_preference,
            (SELECT name FROM users WHERE id = c.created_by) created_by,
            (SELECT name FROM projects WHERE id = t1.project_id) project_name
    FROM customers as c
        INNER JOIN properties first_unit ON first_unit.id = c.first_preference
        LEFT JOIN properties second_unit ON second_unit.id = c.second_preference
        INNER JOIN floors f1 ON f1.id = first_unit.floor_id
        LEFT JOIN floors f2 ON f2.id = second_unit.floor_id
        INNER JOIN towers t1 ON t1.id = f1.tower_id
        LEFT JOIN towers t2 ON t2.id = f2.tower_id
    WHERE c.status  = 1 ${where}
    ORDER BY c.updated_at DESC;
    `
   
    return await sequelize.query(query, { 
        replacements: {
            userId: userId
        },
        type: sequelize.QueryTypes.SELECT 
    });

}


customerDao.getAllotedUnits = async (userId) => {

    let query = `
    SELECT  c.id as customer_id,  
            c.name ,  
            c.phone ,  
            c.broker_name ,  
            c.sourcing ,  
            c.value_in_crs,  
            c.last_four_digit ,  
            c.closing_manager ,
            c.created_at , 
            c.alloted_at,
            c.sold,
            unit.id as unit_id,
            unit.name as alloted_unit,
            p.name as project_name,
            (SELECT name FROM users WHERE id = c.alloted_by) AS alloted_by,
            (SELECT name FROM users WHERE id = c.created_by) AS created_by 
   
    FROM projects as p
        INNER JOIN towers as t ON t.project_id = p.id
        INNER JOIN floors as f ON f.tower_id = t.id
        INNER JOIN properties as unit ON unit.floor_id = f.id
        LEFT JOIN (
                    SELECT min(id) as id, alloted_unit
                    FROM customers
                    WHERE status  = ${STATUS.ACTIVE}
                    GROUP BY alloted_unit
                ) as first_customer
                ON first_customer.alloted_unit = unit.id
        LEFT JOIN customers as c ON c.id = first_customer.id
    ORDER BY ifnull(c.alloted_at,c.created_at) DESC;
    `
    // if(userId != 2){
    // }
   
    return await sequelize.query(query, { 
        replacements: {
            userId: userId
        },
        type: sequelize.QueryTypes.SELECT 
    });

}

customerDao.getCustomerData = async (customerId) => {
    
        return await Models.Customer.findOne({
            where: {
                id: customerId
            },
            raw: true
        });
}

customerDao.updateCustomerData = async (params) => {
    return await Models.Customer.update(params, {
        where: { id: params.id },
    });
}


/**
 * @description A matrix of plot and floor,it's cells will be filled up
 *              with no. of cutomer by whom it was selected
 * @param {*} params 
 * @returns  Array of object
 */
 customerDao.getPlotDashboard = async (params) => {

    if(!params.project) return [];
    
    Models.Towers.hasMany(Models.Floors, {
        foreignKey: 'tower_id',
    });
    Models.Floors.belongsTo(Models.Towers);

    Models.Floors.hasMany(Models.Properties, {
        foreignKey: 'floor_id',
    });
    Models.Properties.belongsTo(Models.Floors);


    Models.Properties.hasMany(Models.Customer, {
        foreignKey: 'unit_id',
    });
    Models.Customer.belongsTo(Models.Properties);

    //Query to get maximium no. of units in a floor
    //to use in render column in dashboard
    let query = `
        SELECT max(x.unit_count) as max_unit, x.type
        FROM (
            SELECT count(1) as unit_count, p.type
            FROM projects p
                INNER JOIN towers t ON p.id = t.project_id 
                    AND t.status = ${STATUS.ACTIVE} 
                    AND p.id = ${params.project}
                    AND p.status = ${STATUS.ACTIVE}
                INNER JOIN floors f ON f.tower_id = t.id
                    AND f.status = ${STATUS.ACTIVE} 
                INNER JOIN properties as unit ON unit.floor_id = f.id
                    AND unit.status = ${STATUS.ACTIVE}  
            GROUP BY p.id,t.id,f.id,p.type
        ) as x
        GROUP BY x.unit_count,x.type;`

    let project = await sequelize.query(query, {type: QueryTypes.SELECT});
    project = project[0];


    //Getting data for dashboard
    let rawValue = (project.type == PROJECT_TYPE.INDEPENDENT_FLOORS) ? true : false;
    let data = await Models.Floors.findAll({
        attributes: [
            [sequelize.literal(`(SELECT name FROM towers WHERE towers.id = floor.tower_id)`), 'tower_name'],
            ['name', 'floor_name']
        ],
        include: {
            model: Models.Properties,
            required: true,
            attributes: [
                'name', 
                [sequelize.literal(`(SELECT count(*) FROM customers WHERE first_preference = properties.id OR second_preference = properties.id)`), 'count'],
                [sequelize.literal(`IF((SELECT sum(sold) FROM customers WHERE alloted_unit = properties.id), 1, 0)`), 'sold']
            ],
            where: {
                status: STATUS.ACTIVE
            },
            
        },
        where: {
            [Op.and]: [
                { status: STATUS.ACTIVE },
                sequelize.where(sequelize.literal(`(SELECT project_id FROM towers WHERE towers.id = floor.tower_id)`),params.project),
            ]
            
        },
        raw: rawValue,
    });

    if(project.type == PROJECT_TYPE.INDEPENDENT_FLOORS) {
        data = _.groupBy(data,'tower_name');
        data = Object.values(data);
        data.forEach(element => {

            for(let i = 0; i < element.length; i++){
                let arr = [];
                element[i].name = element[i]['properties.name'];
                element[i].sold = element[i]['properties.sold'];
                element[i].count = element[i]['properties.count'];
                delete element[i]['properties.name'];
                delete element[i]['properties.sold'];
                delete element[i]['properties.count'];    
            }
        });

    } else {
        data = { data: [...data], maxUnit:project.max_unit };
    }
    return data;
}

module.exports = customerDao;