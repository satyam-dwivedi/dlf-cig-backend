const Models = require("_/model"); 
const sequelize = require("_/config/dbConfig").sequelize;
const Op = require("sequelize").Op;
const {STATUS} = require("_/constants");

const UserDao = {};

UserDao.updateOtp = async (params) => {
    let condition = {
        where: {
            email: params.email
        },
    };
    let update = {
        otp: params.otp,
        otp_sent_time: params.otp_sent_time
    }
    return Models.Users.update(update, condition);
}

UserDao.login = async (params) => {
    let where = {};
    where.email = params.email;
    where.otp = params.otp;
    return await Models.Users.findOne({where});
}

UserDao.getUserDetails = async (params) => {
    let where = {};
    if(params.email)
    where.email = params.email;
    if(params.id)
    where.id = params.id;
  //  (params.email)?where.email = params.email:where.id = params.id;
    return await Models.Users.findOne({where,raw:true});
}

//send user roles at send email time
UserDao.getUserRoleDetails = async (params) => {
    let query = {
        attributes: [[sequelize.fn('GROUP_CONCAT', sequelize.col('roles_id')), 'roles']],
        where: {
            email: params.email,
            status:STATUS.ACTIVE
        },
        raw: true
    }
    return await Models.Users.findAll(query)
}


UserDao.addUser = async (params) => {
    if(params.action == "add")
    return await Models.Users.create(params);
    
    return await Models.Users.bulkCreate(params);
    // return await Models.Users.create(params);
}

//get email of the provided ids for sending mails
UserDao.getUserEmails = async (params) => {
    let condition = {
        attributes: [[sequelize.fn('GROUP_CONCAT', sequelize.col('email')), 'emails']],
        where: {
            id:{
                [Op.in]: params.user_id.split(",")
            },
        status: STATUS.ACTIVE
        },
        raw: true
    }
    return await Models.Users.findAll(condition);
}


module.exports = UserDao;