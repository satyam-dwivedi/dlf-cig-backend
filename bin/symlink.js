/**
 * preinstall
 *
 * creates a symlink in `node_modules` to allow non-relative local `require`
 *   require('_/foo'); // loads {root}/lib/foo/index.js
 *
 * set as npm preinstall command
 *   { "scripts": "preinstall": "./bin/preinstall"}
 */
const fs = require("fs");
const path = require("path");

const symlink = "_";
const libDir = path.join(__dirname, "..", "lib");
const reqDir = path.join(__dirname, "..", "node_modules");
const symLinkPath = path.join(reqDir, symlink);

if (!fs.existsSync(symLinkPath)) {
  fs.symlinkSync(libDir, symLinkPath, "dir");
}
